# Unity Editor Utilities
a collection Unity scripts to extend the unity editor.

to include in a project

- add `io.github.jango-fx.editorutilities": "https://github.com/jango-fx/Unity-Editor-Utilities-Package.git` to your projects package manifest

or

- select `Add package from git URL…` from the Package Manager and use `https://github.com/jango-fx/Unity-Editor-Utilities-Package.git` as URL.


## about
this is a a work in progress.
it's a growing collection of unity scripts that i develop for my own personal work.

## credits
nothing in here is solely my creation. most of it is at least partly inspired by what others publish online.
therefore i'll try to credit all of my references.
should i have missed something: i'm sorry and happy to add credit, where credit is due.

## license
creativecommons share-alike
