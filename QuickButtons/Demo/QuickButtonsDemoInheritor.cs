// ----------------------------------------------------------------------------
// Author: Ryan Hipple
// Date:   06/23/2019
// ----------------------------------------------------------------------------

using UnityUtils.EditorUtilities.QuickButtons;

namespace RoboRyanTron.QuickButtons.Demo
{
    /// <summary>
    /// Class demonstrating that QuickButtons are inherited in inspectors for
    /// subclasses.
    /// </summary>
    public class QuickButtonsDemoInheritor : QuickButtonsDemo
    {}
}