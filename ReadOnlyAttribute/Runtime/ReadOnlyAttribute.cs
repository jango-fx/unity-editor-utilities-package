// ---------------------------------------------------------------------------- 
// based on https://www.patrykgalach.com/2020/01/20/readonly-attribute-in-unity-editor/
// ----------------------------------------------------------------------------

using UnityEngine;

namespace UnityUtils.EditorUtilities
{    
    public class ReadOnlyAttribute : PropertyAttribute { }
}   